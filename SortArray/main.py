nums = []
num_str = input('Input an integer (0 terminates): ')
num_int = int(num_str)
nums.append(num_int)

while num_int != 0:
    num_str = input('Input an integer (0 terminates): ')
    num_int = int(num_str)
    nums.append(num_int)

# Sort method for list type
#nums.sort()

# Bubble sort function (manual)
def sort_list (list):
    list_size = len(list)
    for i in range(list_size):
        already_sorted = True
        for j in range(list_size - i - 1):
            if list[j] > list[j+1]:
                list[j], list[j+1] = list[j+1], list[j]
                already_sorted = False
        if already_sorted:
            break
    return list

nums = sort_list(nums)

# List print
print(nums)

# Print using for i
#for i in range(len(nums)):
#    print(nums[i], end=', ')

# Print using foreach
#for num in nums:
#    print(num, end=', ')