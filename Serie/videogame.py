class Videogame(object):

    def __init__(self, title="", estimated_hours=10, genre="", company=""):
        self._title = title
        self._estimated_hours = estimated_hours
        self._delivered = False
        self._genre = genre
        self._company = company

    def get_title(self):
        return self._title

    def set_title(self, title):
        if title:
            self._title = title

    def get_estimated_hours(self):
        return self._estimated_hours

    def set_estimated_hours(self, estimated_hours):
        if estimated_hours > 0:
            self._estimated_hours = estimated_hours

    def get_genre(self):
        return self._genre

    def set_genre(self, genre):
        if genre:
            self._genre = genre

    def get_company(self):
        return self._company

    def set_company(self, company):
        if company:
            self._company = company

    def deliver(self):
        self._delivered = True

    def is_delivered(self):
        return self._delivered

    def to_string(self):
        return "{title: " + self._title + ", estimated hours: " + str(self._estimated_hours) + ", delivered: " + \
               str(self._delivered) + ", genre: " + self._genre + ", company: " + self._company + "}"
