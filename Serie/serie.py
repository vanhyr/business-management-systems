class Serie(object):

    def __init__(self, title="", seasons_number=3, genre="", creator=""):
        self._title = title
        self._seasons_number = seasons_number
        self._delivered = False
        self._genre = genre
        self._creator = creator

    def get_title(self):
        return self._title

    def set_title(self, title):
        if title:
            self._title = title

    def get_seasons_number(self):
        return self._seasons_number

    def set_seasons_number(self, seasons_number):
        if seasons_number > 0:
            self._seasons_number = seasons_number

    def get_genre(self):
        return self._genre

    def set_genre(self, genre):
        if genre:
            self._genre = genre

    def get_creator(self):
        return self._creator

    def set_creator(self, creator):
        if creator:
            self._creator = creator

    def deliver(self):
        self._delivered = True

    def is_delivered(self):
        return self._delivered

    def to_string(self):
        return "{title: " + self._title + ", number of seasons: " + str(self._seasons_number) + ", delivered: " + \
               str(self._delivered) + ", genre: " + self._genre + ", creator: " + self._creator + "}"
