from serie import Serie
from videogame import Videogame


def show_delivered(series, videogames):
    delivered_series = 0
    delivered_videogames = 0
    print(f"\n----------------[Delivered]----------------\n")
    for serie in series:
        if serie.is_delivered():
            delivered_series += 1
            print(f"{serie.to_string()}")
    for videogame in videogames:
        if videogame.is_delivered():
            delivered_videogames += 1
            print(f"{videogame.to_string()}")
    print(f"\n----------------[Delivered count]----------------\n")
    print(f"Series: {delivered_series}")
    print(f"Video games: {delivered_videogames}")


def show_biggest(series, videogames):
    series = sorted(series, key=lambda serie: serie.get_seasons_number(), reverse=True)
    videogames = sorted(videogames, key=lambda videogame: videogame.get_estimated_hours(), reverse=True)
    print(f"\n----------------[Top seasons serie]----------------\n")
    print(f"{series[0].to_string()}")
    print(f"\n----------------[Top hours video game]----------------\n")
    print(f"{videogames[0].to_string()}")


if __name__ == '__main__':
    series = [
        Serie("Breaking Bad", 5, "serial drama/crime drama/thriller/neo-western/black comedy/tragedy",
              "Vince Gilligan"),
        Serie("Vikings", 6, "historical drama/action-adventure", "Michael Hirst"),
        Serie("Narcos", 3, "crime drama/biographical", "Chris Brancato, Carlo Bernard, Doug Miro"),
        Serie("Peaky Blinders", 6, "historical fiction/crime drama", "Steven Knight"),
        Serie("Sons of Anarchy", 7, "action/crime drama/tragedy/neo-western", "Kurt Sutter"),
    ]
    videogames = [
        Videogame("Smite", 2000, "MOBA", "Hi-Rez Studios/Titan Forge Games"),
        Videogame("Half-Life", 12, "first-person shooter", "Valve/Sierra Studios"),
        Videogame("Dark Souls", 46, "action role-playing", "FromSoftware/Namco Bandai Games"),
        Videogame("The Elder Scrolls V: Skyrim", 30, "action role-playing", "Bethesda"),
        Videogame("Grand Theft Auto V", 30, "action-adventure", "Rockstar"),
    ]
    series[0].deliver()
    series[1].deliver()
    series[3].deliver()
    videogames[0].deliver()
    videogames[1].deliver()
    videogames[2].deliver()
    show_delivered(series, videogames)
    show_biggest(series, videogames)
