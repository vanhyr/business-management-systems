from dataclasses import dataclass


@dataclass
class Cat:
    """
    Data class for a cat.
    """

    name: str = ""
    color: str = ""
    age: int = 0
    weight: float = 0
