from model.cat import Cat


class CatController:
    """
    Class for managing a list of cats objects.
    """

    def __init__(self):
        """
        Creates a new CatController object with an empty list of cats.

        Parameters:
            None.

        Returns:
            Nothing.
        """
        self._cats = []

    def add(self, cat: Cat):
        """
        Adds a cat into the cats list if it doesn't exists already.

        Parameters:
            cat (Cat): A Cat object.

        Returns:
            Nothing.
        """
        if cat not in self._cats:
            self._cats.append(cat)

    def remove(self, cat: Cat):
        """
        Deletes a cat from the cats list.

        Parameters:
            cat (Cat): A Cat object.

        Returns:
            Nothing.
        """
        self._cats.remove(cat)

    def update(self, cat_previous: Cat, cat_update: Cat):
        """
        Updates a cat from the cats list if it exists.

        Parameters:
            cat_previous (Cat): The cat to update.
            cat_update (Cat): The updated cat.

        Returns:
            Nothing.
        """
        if cat_previous != cat_update:
            for i, cat in enumerate(self._cats):
                if cat_previous == cat:
                    self._cats[i] = cat_update

    def get_cats(self):
        """
        Get the cats list.

        Parameters:
            None.

        Returns:
            The list of cats.
        """
        return self._cats

    def set_cats(self, cats: list):
        """
        Sets the list of cats.

        Parameters:
            None.

        Returns:
            The list of cats.
        """
        self._cats = cats
