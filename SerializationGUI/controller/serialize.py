import pickle
from os.path import exists
from pathlib import Path


class Serialize:
    """
    Class for managing serialization operations into a file.
    """

    def __init__(self, file_name: str):
        """
        Creates a new Serialize object for the given file name (stored in the /res folder inside the project).

        Parameters:
            file_name (str): The file name (stored in the /res folder inside the project).

        Returns:
            Nothing.
        """
        self._file_uri = Path(__file__).parent / "../res/db" / file_name

    def read(self):
        """
        Reads the file and stores its content into a list.

        Parameters:
            None.

        Returns:
            The file contents as a list.
        """
        if exists(self._file_uri):
            try:
                # Since python 3
                # Open the file in read binary mode
                with open(self._file_uri, "rb") as file:
                    # Deserializes and loads the given file content into a list
                    content = pickle.load(file)
                # Close the file
                file.close()
                return content
            except OSError:
                print("OSError, couldn't open file")
            except EOFError:
                print("EOFError, file is empty")
            except pickle.UnpicklingError:
                print("Error pickle")

    def write(self, object_list):
        """
        Write the object list into the file (creates it if it doesn't exists and overwrites it everytime it's called).

        Parameters:
            object_list (list): The list of objets to be stored in the file.

        Returns:
            Nothing.
        """
        try:
            # Since python 3
            # Open the file in write binary mode
            with open(self._file_uri, "wb") as file:
                # Serialize and add to the given file the object list
                pickle.dump(object_list, file)
            # Close the file
            file.close()
        except OSError:
            print("OSError, couldn't open file")
        except EOFError:
            print("EOFError, file is empty")
        except pickle.UnpicklingError:
            print("Error pickle")
