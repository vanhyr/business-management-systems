from controller.serialize import Serialize
from model.cat import Cat
from controller.cat_controller import CatController
from tkinter import *
from tkinter import ttk, simpledialog, messagebox
from tkinter.messagebox import askyesno

from pathlib import Path


class MainView:

    _window: Tk
    _file_name: str
    _table: ttk.Treeview
    _entry_filename: Entry
    _entry_name: Entry
    _entry_color: Entry
    _entry_age: Entry
    _entry_weight: Entry
    _name: str
    _color: str
    _age: int
    _weight: float
    _table_name: str
    _table_color: str
    _table_age: int
    _table_weight: float
    _cat_controller: CatController
    _serialize: Serialize

    def __init__(self, title: str, dimensions: str, bg: str):
        # Create tkinter main window
        self._window = Tk()

        # Set window options
        self._window.wm_title(title)
        self._window.geometry(dimensions)

        if bg:
            self._window["bg"] = bg

        # Initialize style
        style = ttk.Style()

        # Load external theme
        self._window.tk.call('lappend', 'auto_path', Path(__file__).parent / "../res/themes/breeze-dark")
        self._window.tk.call('package', 'require', 'ttk::theme::breeze-dark')
        # Set the theme we loaded
        style.theme_use("breeze-dark")

        # Configure the style
        style.configure("Treeview", fieldbackground="#333333", rowheight=20)
        style.configure("Treeview.Heading", font=("Cantarell", 12), foreground="white", background="#333333")
        style.map('Treeview.Heading', background=[("active", "#444444")])
        style.configure("Vertical.TScrollbar", background="#333333", arrowcolor="white")
        style.map("Vertical.TScrollbar", background=[("pressed", "#333333"), ("active", "#333333")])

        self._add_scroll(frame=self._window)

        # file_name = self._ask_file_name()
        self._file_name = "cats.db"

        #########################################################

        # Get the list from the file
        self._cat_controller = CatController()
        # self._serialize = Serialize(file_name)
        self._serialize = Serialize(self._file_name)
        cats = self._serialize.read()
        if cats:
            self._cat_controller.set_cats(cats)

        # Get attributes of the Class (needs an object)
        # attributes = list(Cat().__dict__.keys())
        # Get attributes of the Class (needs default values)
        attributes = [key for key in Cat.__dict__.keys() if not key.startswith('_')]

        ##########################################################

        menu_frame = self._add_frame()
        self._label(frame=menu_frame, text="Database file:")
        self._entry_filename = self._entry(frame=menu_frame)
        self._entry_filename.insert(0, self._file_name)
        #self._entry_filename.config(validate="focusout", validatecommand=self._check_filename,
        #                             invalidcommand=self._wrong_filename)
        self._button(frame=menu_frame, text="Change", command=self._change_filename)

        # Load a dynamic table (independent of the object or the list)
        # load_table(window, attributes, cats)
        self._load_table(attributes, cats)
        self._load_input()
        self._load_buttons()

    def _add_scroll(self, frame: {Tk, Frame}, orient: str = "vertical"):
        scrollbar = ttk.Scrollbar(frame, orient=orient)
        if orient == "vertical":
            scrollbar.pack(side=RIGHT, fill=Y)
        elif orient == "horizontal":
            scrollbar.pack(side=BOTTOM, fill=X)
        return scrollbar

    def _add_frame(self, bg: str = "#333333"):
        # Create the table frame
        frame = Frame(self._window)
        # Position the table frame
        frame.pack(pady=20)
        if bg:
            frame["bg"] = bg
        return frame

    def _change_filename(self):
        if self._check_filename():
            self._serialize = Serialize(self._file_name)
            cats = self._serialize.read()
            if cats:
                self._cat_controller.set_cats(cats)
            else:
                self._cat_controller.set_cats([])
            self._clear_table()
            self._fill_rows(cats)
        else:
            self._wrong_filename()

    def _check_filename(self):
        filename = self._entry_filename.get()
        if filename.endswith(".db"):
            if not all(char.isalpha() or char == "." for char in filename):
                return False
            self._file_name = filename
            return True
        return False

    def _wrong_filename(self):
        messagebox.showwarning("Invalid file name", "Please enter a valid file name (must end with .db) eg: filename.db")
        self._entry_filename.delete(0, "end")
        self._entry_filename.insert(0, self._file_name)
        self._entry_filename.focus_set()

    def _fill_rows(self, objs: list):
        if objs:
            # Insert the data rows in the Treeview widget
            for i, obj in enumerate(objs):
                values = []
                for attr in list(obj.__dict__.keys()):
                    values.append(obj.__getattribute__(attr))
                # Distinct between odd and even rows
                if i % 2 == 0:
                    self._table.insert("", "end", text=str(i + 1), values=values, tags="odd")
                else:
                    self._table.insert("", "end", text=str(i + 1), values=values, tags="even")

    # CREATE CLASS TKINTER_MANAGER AND PUT ALL TKINTER RELATED CODE THERE
    # CREATE METHODS FOR BUTTONS AND VARIABLE FOR REPEATING VALUES SUCH AS bg="#333333"
    def _load_table(self, attrs: list, objs: list):
        """
        Loads the tkinter table dynamically for the given object list and its attributes.

        Parameters:
            attrs (list): A list of attributes for the object that will be displaying on the table (the heading values).
            objs (list): A list of objects to display as rows in the table.

        Returns:
            Nothing.
        """
        table_frame = self._add_frame()

        # Table scrollbar
        table_scrollbar = self._add_scroll(table_frame)

        # Creating number of columns
        columns = []
        for i, attr in enumerate(attrs):
            columns.append("#" + str(i + 1))

        # Create the Treeview widget (the table)
        self._table = ttk.Treeview(table_frame, column=columns, show="headings", height=8,
                                   selectmode="browse", yscrollcommand=table_scrollbar.set)
        # table = ttk.Treeview(table_frame, column=columns, show="headings", height=len(objs),
        #                      yscrollcommand=table_scrollbar.set)
        # Position the table inside the frame
        self._table.pack(side=LEFT, fill=BOTH)

        self._table.bind("<<TreeviewSelect>>", self._on_select)

        # Set the look for odd and even cells (using tags)
        self._table.tag_configure("odd", font=("Cantarell", 12), foreground="white", background="#333333")
        self._table.tag_configure("even", font=("Cantarell", 12), foreground="white", background="#444444")

        # Set functionality to the table scrollbar
        table_scrollbar.config(command=self._table.yview)

        # Creating heading for each attribute
        for i, attr in enumerate(attrs):
            self._table.heading("#" + str(i + 1), text=attr.capitalize())

        self._fill_rows(objs)

    def _entry(self, frame: Frame, bg: str = "#333333", width: int = 30):
        if not frame:
            entry = Entry(self._window, bg=bg, width=width)
        else:
            entry = Entry(frame, bg=bg, width=width)
        entry.pack(side=LEFT)
        return entry

    def _label(self, frame: Frame, text: str = "Label", bg: str = "#333333", padx: int = 30):
        if not frame:
            label = Label(self._window, text=text, bg=bg)
        else:
            label = Label(frame, text=text, bg=bg)
        label.pack(side=LEFT, padx=padx)
        return label

    def _load_input(self):
        input_frame = self._add_frame(bg="#333333")

        self._label(input_frame, text="Name", bg="#333333")
        self._entry_name = self._entry(input_frame, bg="#333333", width=40)
        # self._entry_name.config(validate="focusout", validatecommand=self._check_name,
        #                         invalidcommand=self._wrong_name)

        self._label(input_frame, text="Color", bg="#333333")
        self._entry_color = self._entry(input_frame, bg="#333333", width=30)
        # self._entry_color.config(validate="focusout", validatecommand=self._check_color,
        #                          invalidcommand=self._wrong_color)

        self._label(input_frame, text="Age", bg="#333333")
        self._entry_age = self._entry(input_frame, bg="#333333", width=6)
        # self._entry_age.config(validate="focusout", validatecommand=self._check_age,
        #                        invalidcommand=self._wrong_age)

        self._label(input_frame, text="Weight", bg="#333333")
        self._entry_weight = self._entry(input_frame, bg="#333333", width=8)
        # self._entry_weight.config(validate="focusout", validatecommand=self._check_weight,
        #                           invalidcommand=self._wrong_weight)

    def _button(self, command, frame: Frame, text: str = "Button", padx: int = 30):
        if not frame:
            button = Button(self._window, text=text, command=command)
        else:
            button = Button(frame, text=text, command=command)
        button.pack(side=LEFT, padx=padx)
        return button

    def _load_buttons(self):
        buttons_frame = self._add_frame(bg="#333333")

        self._button(frame=buttons_frame, text="Add", command=self._add)
        self._button(frame=buttons_frame, text="Update", command=self._update)
        self._button(frame=buttons_frame, text="Delete", command=self._delete)

    def _add(self):
        self._get_entry_values()
        if self._valid_values():
            self._cat_controller.add(Cat(self._name, self._color, self._age, self._weight))
            self._perform()
        else:
            messagebox.showwarning("Invalid values", "Please check the values are in a valid")

    def _on_select(self, evt: Event):
        self._clear_entries()

        item = self._table.selection()
        if item:
            values = self._table.item(item, "values")

            self._entry_name.insert(0, values[0])
            self._entry_color.insert(0, values[1])
            self._entry_age.insert(0, values[2])
            self._entry_weight.insert(0, values[3])

    def _update(self):
        if self._table.selection():
            self._get_table_values()
            self._get_entry_values()
            if self._valid_values():
                self._cat_controller.update(Cat(self._table_name, self._table_color, self._table_age, self._table_weight),
                                            Cat(self._name, self._color, self._age, self._weight))
                self._perform()
            else:
                messagebox.showwarning("Invalid values", "Please check the values are in a valid")

    def _delete(self):
        if self._table.selection():
            answer = askyesno("Confirm deletion", "Are you sure you want to delete the selected item?")
            if answer:
                self._get_table_values()
                self._cat_controller.remove(Cat(self._table_name, self._table_color, self._table_age, self._table_weight))
                self._perform()

    def _valid_values(self):
        if (self._name and self._valid_name(self._name)
                and self._color and self._valid_color(self._color)
                and self._age and self._valid_age(self._age)
                and self._weight and self._valid_weight(self._weight)):
            return True
        return False

    def _check_name(self):
        if not self._valid_name(self._entry_name.get()):
            return False
        return True

    def _wrong_name(self):
        messagebox.showwarning("Invalid name", "Please enter a valid name (only characters and spaces)")
        self._entry_name.delete(0, "end")
        self._entry_name.focus_set()

    def _valid_name(self, name: str):
        if not all(char.isalpha() or char == "" for char in name):
            return False
        return True

    def _check_color(self):
        if not self._valid_color(self._entry_color.get()):
            return False
        return True

    def _wrong_color(self):
        messagebox.showwarning("Invalid color", "Please enter a valid color (only characters, spaces and /)")
        self._entry_color.delete(0, "end")
        self._entry_color.focus_set()

    def _valid_color(self, color: str):
        if not all(char.isalpha() or char == "" or char == "/" for char in color):
            return False
        return True

    def _check_age(self):
        if not self._valid_age(self._entry_age.get()):
            return False
        return True

    def _wrong_age(self):
        messagebox.showwarning("Invalid age", "Please enter a valid age (only numbers and > 0)")
        self._entry_age.delete(0, "end")
        self._entry_age.focus_set()

    def _valid_age(self, age: int):
        try:
            age = int(age)
            if age > 0:
                return True
        except ValueError:
            return False

    def _check_weight(self):
        if not self._valid_weight(self._entry_weight.get()):
            return False
        return True

    def _wrong_weight(self):
        messagebox.showwarning("Invalid weight", "Please enter a valid weight (only numbers and > 0)")
        self._entry_weight.delete(0, "end")
        self._entry_weight.focus_set()

    def _valid_weight(self, weight: float):
        try:
            weight = float(weight)
            if weight > 0:
                return True
        except ValueError:
            return False

    def _perform(self):
        cats = self._cat_controller.get_cats()
        self._serialize.write(cats)
        self._clear_table()
        self._fill_rows(self._serialize.read())
        self._clear_entries()

    def _get_table_values(self):
        item = self._table.selection()
        if item:
            values = self._table.item(item, "values")

            self._table_name = values[0]
            self._table_color = values[1]
            self._table_age = values[2]
            self._table_weight = values[3]

    def _get_entry_values(self):
        self._name = self._entry_name.get()
        self._color = self._entry_color.get()
        self._age = self._entry_age.get()
        self._weight = self._entry_weight.get()

    def _clear_entries(self):
        self._entry_name.delete(0, "end")
        self._entry_color.delete(0, "end")
        self._entry_age.delete(0, "end")
        self._entry_weight.delete(0, "end")

    def _clear_table(self):
        for row in self._table.get_children():
            self._table.delete(row)

    def _ask_file_name(self):
        while True:
            try:
                file_name = simpledialog.askstring('File name',
                                                   'Enter the file name for storing the data (must end with .db)')
                if file_name.endswith(".db"):
                    return file_name
            except AttributeError:
                self._window.destroy()
                break

    def show(self):
        # Show window
        self._window.mainloop()


def main():
    main_view = MainView(title="Serialize objects", dimensions="1000x700", bg="#333333")
    main_view.show()


if __name__ == "__main__":
    main()
