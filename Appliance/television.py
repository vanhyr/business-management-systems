from appliance import Appliance


class Television(Appliance):

    def __init__(self, base_price=100, weight=5, resolution=20, four_k=False):
        super().__init__(base_price, weight)
        self._resolution = resolution
        self._four_k = four_k

    def get_resolution(self):
        return self._resolution

    def get_four_k(self):
        return self._four_k

    def final_price(self):
        final_price = super().final_price()
        if self._resolution > 40:
            final_price += super().get_base_price() % 30
        if self._four_k:
            final_price += 50
        return final_price
