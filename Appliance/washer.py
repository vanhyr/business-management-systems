from appliance import Appliance


class Washer(Appliance):
    _CHARGE = 5

    def __init__(self, charge=5):
        super().__init__()
        self._CHARGE = charge

    def get_charge(self):
        return self._CHARGE

    def final_price(self):
        final_price = super().final_price()
        if self._CHARGE > 30:
            final_price += 50
        return final_price
