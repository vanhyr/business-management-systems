class Appliance(object):

    def __init__(self, base_price=100, color="white", energy_consumption="F", weight=5):
        self._base_price = base_price
        self._check_color(color)
        self._check_energy_consumption(energy_consumption)
        self._weight = weight

    def get_base_price(self):
        return self._base_price

    def get_color(self):
        return self._color

    def get_energy_consumption(self):
        return self._energy_consumption

    def get_weight(self):
        return self._weight

    def _check_energy_consumption(self, letter):
        letters = [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
        ]
        for le in letters:
            if le == letter:
                self._energy_consumption = letter
            else:
                self._energy_consumption = "F"

    def _check_color(self, color):
        colors = [
            "white",
            "White",
            "black",
            "Black",
            "red",
            "Red",
            "blue",
            "Blue",
            "grey",
            "Grey",
        ]
        for c in colors:
            if c == color:
                self._color = color
            else:
                self._color = "white"

    def final_price(self):
        final_price = self._base_price
        letter = self._energy_consumption
        weight = self._weight
        if letter == "A":
            final_price += 100
        elif letter == "B":
            final_price += 80
        elif letter == "C":
            final_price += 60
        elif letter == "D":
            final_price += 50
        elif letter == "E":
            final_price += 30
        elif letter == "F":
            final_price += 10
        if 0 < weight < 20:
            final_price += 10
        if 20 < weight < 49:
            final_price += 50
        if 50 < weight < 80:
            final_price += 80
        if weight > 80:
            final_price += 100
        return final_price
