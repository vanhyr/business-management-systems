from appliance import Appliance
from washer import Washer
from television import Television


def show_final_prices(appliances):
    total_appliance = 0
    total_washer = 0
    total_television = 0
    for appliance in appliances:
        class_name = type(appliance).__name__
        if class_name == "Appliance":
            total_appliance += appliance.final_price()
        elif class_name == "Washer":
            total_washer += appliance.final_price()
        elif class_name == "Television":
            total_television += appliance.final_price()
    total = total_appliance + total_washer + total_television
    print(f"\n----------------[Total final prices]-----------------\n")
    print(f"Total: {total}")
    print(f"Appliances: {total_appliance}")
    print(f"Washers: {total_washer}")
    print(f"Televisions: {total_television}")


if __name__ == '__main__':
    appliances = [
        Appliance(),
        Washer(40),
        Appliance(200, "Red", "A", 30),
        Appliance(500, energy_consumption="D", weight=80),
        Television(),
        Appliance(800, weight=40),
        Television(resolution=50, four_k=True),
        Television(1200, 20, 66, True),
        Washer(),
        Appliance(color="Blue"),
    ]
    show_final_prices(appliances)
