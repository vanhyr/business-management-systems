from math import pow
from random import randint


class Person(object):

    def __init__(self, name="", age=0, gender="W", weight=0.0, height=0.0):
        self._name = name
        self._age = age
        self._dni = self._generate_dni()
        if gender == "M":
            self._gender = gender
        elif gender == "W":
            self._gender = gender
        # else:
        #    self._gender == "W"
        self._weight = weight
        self._height = height

    def set_name(self, name):
        if name:
            self._name = name

    def get_name(self):
        return self._name

    def set_age(self, age):
        if age > 0:
            self._age = age

    def set_dni(self, dni):
        if dni:
            self._dni = dni

    def set_gender(self, gender):
        if gender:
            if gender == "M":
                self._gender = gender
            elif gender == "W":
                self._gender = gender

    def set_weight(self, weight):
        if weight > 0:
            self._weight = weight

    def set_height(self, height):
        if height > 0:
            self._height = height

    def calculate_cmi(self):
        if self._weight > 0 and self._height > 0:
            health = 0
            cmi = self._weight / pow(self._height, 2)
            if cmi < 18.5:
                health = -1
            elif cmi > 25:
                health = 2
            return health, cmi
        else:
            return None, None

    def of_legal_age(self):
        if self._age > 18:
            return True
        return False

    def insert_gender(self, gender="W"):
        if gender:
            if gender == "M":
                self._gender = gender
            elif gender == "W":
                self._gender = gender

    def _generate_dni(self):
        dni = ""
        for i in range(8):
            dni += str(randint(0, 9))
        letters = "TRWAGMYFPDXBNJZSQVHLCKE"
        number = int(dni) % 23
        letter = letters[number]
        dni += str(letter)
        return dni

    def to_string(self):
        return "{name: " + self._name + ", age: " + str(self._age) + ", dni: " + self._dni + ", gender: " \
               + self._gender + ", weight: " + str(self._weight) + ", height: " + str(self._height) + "}"
