from person import Person


def show_info(person):
    health, cmi = person.calculate_cmi()
    print(f"\n----------------[{person.get_name()}]----------------\n")
    if health == -1:
        print(f'Weight is under the recommended')
    elif health == 0:
        print(f'Weight is correct')
    elif health == 2:
        print(f'Has overweight')
    if person.of_legal_age():
        print(f'Is of legal age')
    else:
        print(f'Is not of legal age')
    print(person.to_string())


if __name__ == '__main__':
    name = input('Input the name: ') or ""
    age = int(input('Input the age: ') or 0)
    gender = input('Input the gender [M/W]: ') or ""
    weight = float(input('Input the weight in kg: ') or 0.0)
    height = float(input('Input the height in meters: ') or 0.0)
    p1 = Person(name, age, gender, weight, height)
    p2 = Person(name, age, gender)
    p3 = Person()
    p3.set_name("Valentin")
    p3.set_age(25)
    p3.set_gender("M")
    p3.set_weight(72)
    p3.set_height(1.84)
    show_info(p1)
    show_info(p2)
    show_info(p3)
